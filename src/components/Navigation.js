import React, { useState } from "react"
import { Link } from "react-router-dom";

function Navigation () {
  
  const [isExpanded, toggleExpansion] = useState(false)
  
  return (
    <div className="header__navigation grid__item--md-66 d-flex justify--end">
      <button className={`navigation__toggler ${ isExpanded ? `is-open` : `is-closed` }`} type="button" data-toggle="navigation" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation" onClick={() => toggleExpansion(!isExpanded)} >
        <span className="sr-only">Toggle Navigation</span>
          <span className="hamburger">
            <span className="hamburger__line hamburger__line--1"></span>
            <span className="hamburger__line hamburger__line--2"></span>
            <span className="hamburger__line hamburger__line--3"></span>
            <span className="hamburger__line hamburger__line--4"></span>
          </span>
      </button>
      <nav aria-label="primary" className={`navigation ${ isExpanded ? `is-open` : `is-closed` }`} id="navigation">
      <Link to="/" className="navigation__link">Home</Link>
      <Link to="/search" className="navigation__link">Search</Link>
      <Link to="/loved-videos" className="navigation__link">Loved Videos</Link>
      </nav>
    </div>
  )
}
export default Navigation