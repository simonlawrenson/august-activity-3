import React from "react"
import Navigation from "./Navigation"
import { Link } from "react-router-dom";

const Header = ({ siteTitle }) => (
  <header className="header">
    <div className="container grid__container">
      <div className="header__site-branding grid__item--md-33">
        <h1 className="header__site-title">
          <Link to="/" className="navigation__link">Youtube API</Link>
        </h1>
        <p className="header__site-tagline">Activity 3.</p>
      </div>
      <Navigation />
    </div>
  </header>
)

export default Header
