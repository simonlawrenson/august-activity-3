export const AppReducer = (state, action) => {

  switch(action.type) {
    case 'FAVOURITE_VIDEO':
      if( !state.find(video => video.id === action.id) ) {
        return [
          ...state, {
            id: action.id,
            videoJSON: action.videoJSON,
            title: action.title,
            description: action.description,
            img: action.img
          }
        ]
      } else {
        return state.filter( video => video.id !== action.id )
      }
    // case 'BTN_STATE':
    // case 'REMOVE_VIDEO' :
      // return state.filter( video => video.id !== action.id )
    default:
      return state
  }
}