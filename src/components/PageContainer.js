import React from "react"

export default ({ children }) => (
  <main className="container container--page">
    {children}
  </main>
)