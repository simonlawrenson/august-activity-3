import React, {Component} from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../utils/fontawesome'

class IndexPage extends Component {
  render() {
    return(
      <div>
        <header className="container--header">
          <div className="grid__container">
            <div className="grid__item">
              <h1>Welcome Augie <FontAwesomeIcon icon={['fal', 'hand-paper']} /></h1>
              <p>You're now viewing Simon Lawrenson's attempt at Activity 3 for August.</p>
              <p>This app allows you to search for your favourite videos via the Youtube API.</p>
              <p>You can curated a list of your favourite videos and come back to watch them later.</p>
              <p>Thanks, Simon.</p>
            </div>
          </div>
        </header>
      </div>
    )
  }
}

export default IndexPage

