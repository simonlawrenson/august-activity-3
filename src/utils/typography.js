import Typography from "typography"

const typography = new Typography({
  baseFontSize: "18px",
  baseLineHeight: 1.666,
  headerFontFamily: ["Playfair Display", "serif"],
  bodyFontFamily: ["SpaceGrotesk", "serif"],
})

export default typography