import React from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../utils/fontawesome'

const Footer = () => (
  <footer className="footer">
    <div className="container">
      <p className="footer__links">
        Made with <FontAwesomeIcon icon={['fal', 'heart']} /> by <a href="https://lawrenson.dev" title="Simon Lawrenson">lawrenson.dev</a>
      </p>
    </div>
  </footer>
)

export default Footer