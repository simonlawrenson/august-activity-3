import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../../utils/fontawesome'

const VideoLayout = ({video, isLocal, handleVideoSelect}) => {
  if( isLocal ) {
    video = video.videoJSON
  }
  return (
    <article className="video__item" role="button" onClick={ () => handleVideoSelect(video)}>
      <div className="video__image">
        <img src={video.snippet.thumbnails.medium.url} alt={video.snippet.description}/>
        <span className="video__image__hover d-flex align--center justify--center">
          <FontAwesomeIcon icon={['fal', 'play']} />
        </span>
      </div>
      <header className="video__content">
        <h4 className="video__title">{video.snippet.title}</h4>
        <p className="video__description">{video.snippet.description}</p>
      </header>
    </article>
  )
};
export default VideoLayout;