import React, { Component } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../../utils/fontawesome'

class SearchForm extends Component {
  state = {
    searchTerm: ""
  }

  handleChange = event => {
    this.setState({ searchTerm: event.target.value })
  }

  handleSubmit = event => {
    event.preventDefault();
    this.props.handleFormSubmit(this.state.searchTerm)
  }

  render() {
    return(
      <div className="grid__container container--search-form">
        <div className="grid__item">
          <p>Your search: {this.state.searchTerm}</p>
          <form onSubmit={this.handleSubmit} className="search-form d-flex">
            <input
            type="text"
            name="searchField"
            value={this.state.searchTerm}
            onChange={this.handleChange}
            placeholder="Search for your favourite videos..."
            className="search-form__input"
            />
            <button className="search-form__btn btn--primary">
              <FontAwesomeIcon icon={['fal', 'search']} />
            </button>
          </form>
        </div>
      </div>
    )
  }
}

export default SearchForm