import React, { useContext, useState } from "react"
import { AppContext } from '../../contexts/AppContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../../utils/fontawesome'

const VideoControls = ({video}) => {
  // const { videos, dispatch } = useContext(AppContext);
  const { videos, dispatch } = useContext(AppContext)
  const [isVideoLoved, toggleBtnClass] = useState(() => {
    if( videos.find(list => list.id === video.id.videoId) ) {
      return true
    } else {
      return false
    }
  })

  return(
    <div className="video__controls">
      <button
        className={`btn btn--control ${ isVideoLoved ? `btn--loved` : `` }`}
        onClick={() => {
          dispatch({ type: 'FAVOURITE_VIDEO',
          id: video.id.videoId,
          videoJSON: video,
          title: video.snippet.title,
          description: video.snippet.description,
          img: video.snippet.thumbnails.medium.url
        });
        toggleBtnClass(!isVideoLoved)}}
      >
        <FontAwesomeIcon icon={['fas', 'heart']} />
      </button>
    </div>
  )
}


export default VideoControls  