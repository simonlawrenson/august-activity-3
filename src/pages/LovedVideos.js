import React, {Component} from "react"
import LovedVideosContainer from "../components/LovedVideosContainer"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../utils/fontawesome'

class LovedVideos extends Component {
  render() {
    return(
      <div>
        <header className="container--header">
          <div className="grid__container">
            <div className="grid__item">
              <h1>Your <FontAwesomeIcon icon={['fal', 'heart']} /> Videos</h1>
              <p>Your favourite videos are below. Watch them again.</p>
            </div>
          </div>
        </header>
        <LovedVideosContainer />
      </div>
    )
  }
}

export default LovedVideos