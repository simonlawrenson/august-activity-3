import { library } from '@fortawesome/fontawesome-svg-core'
// import the icons
import { faHeart as faSHeart } from '@fortawesome/free-solid-svg-icons'
// import { faBars } from '@fortawesome/pro-regular-svg-icons';
// import { fab } from '@fortawesome/free-brands-svg-icons';
import {
  faSearch,
  faHeart as faLHeart,
  faPlay,
  faHandPaper
} from '@fortawesome/pro-light-svg-icons';

/* @fortawesome/fontawesome-pro */
library.add(faSearch, faSHeart, faLHeart, faPlay, faHandPaper)