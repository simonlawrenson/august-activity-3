import React, { Component } from "react"
import youtubeApi, { baseParams } from './Player/youtube-api'
import SearchForm from './Player/SearchForm'
import VideoDetail from './Player/VideoDetail'
import VideoList from './Player/VideoList'

class SearchContainer extends Component {
  state = {
    videos : [],
    selectedVideo: null
  }

  handleSubmit = async (searchTerm) => {
    const response  = await youtubeApi.get('/search', {
      params: {
        ...baseParams,
        q: searchTerm
      }
    })
    this.setState({
      videos: response.data.items
    })
  }

  handleVideoSelect = (video) => {
    this.setState({
      selectedVideo : video
    })
  }

  render() {
    return(
      <div className="container--search">
        <SearchForm handleFormSubmit={this.handleSubmit} />
        <VideoDetail video={this.state.selectedVideo}/>
        <VideoList handleVideoSelect={this.handleVideoSelect} videos={this.state.videos} isLocal={false}/>
      </div>
    )
  }
}

export default SearchContainer