import axios from 'axios';

const KEY = process.env.REACT_APP_YT_API_KEY;

export const baseParams = {
    maxResults: 12,
    order: 'viewCount',
    part: 'snippet',
    type: 'video',
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    key: KEY
}

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3/',
})