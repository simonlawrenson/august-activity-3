import React from 'react';
import VideoControls from './VideoControls'

const VideoDetail = ({video}) => {
  if (!video) {
    return (
      <div>
      </div>
    )
  }

  const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`;
  return (
    <div className="grid__container container--video__display">
      <div className="grid__item grid__item--md-66 video__selected">
        <div className="video__selected__inner">
          <iframe src={videoSrc} allowFullScreen title="Video player" />
        </div>
      </div>
      <div className="grid__item grid__item--md-33 video__selected__content">
        <h3 className="video__selected__title">{video.snippet.title}</h3>
        <p className="">{video.snippet.description}</p>
        <VideoControls key={video.id.videoId} video={video} />
      </div>
    </div>
  )
}

export default VideoDetail;