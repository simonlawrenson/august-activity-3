import React, {Component} from "react"
import SearchContainer from "../components/SearchContainer"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../utils/fontawesome'

class Search extends Component {
  render() {
    return(
      <div>
        <header className="container--header">
          <div className="grid__container">
            <div className="grid__item">
              <h1>Search</h1>
              <p>Search, watch or hit the <FontAwesomeIcon icon={['fal', 'heart']} /> to save your favourite videos for later.</p>
            </div>
          </div>
        </header>
        <SearchContainer />
      </div>
    )
  }
}

export default Search