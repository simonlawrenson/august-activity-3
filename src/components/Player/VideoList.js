import React from "react"
import VideoLayout from './VideoLayout'

const VideoList = ({videos, isLocal, handleVideoSelect}) => {
  const renderedVideos = videos.map((video) => {
    return(
      <div className="grid__item grid__item--sm-50 grid__item--md-25 video">
        <VideoLayout key={video.id.videoId} video={video} isLocal={isLocal} handleVideoSelect={handleVideoSelect} />
      </div>
    )
  })

  return(
    <div className="grid__container container--video__list">
      {renderedVideos}
    </div>
  )
}

export default VideoList