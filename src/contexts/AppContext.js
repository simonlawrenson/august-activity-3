import React, { createContext, useReducer, useEffect } from 'react'
import { AppReducer } from '../reducers/AppReducer'

export const AppContext = createContext()
  
const AppContextProvider = (props) => {
  const [videos, dispatch] = useReducer(AppReducer, [], () => {
    const localData = localStorage.getItem('videos')
    return localData ? JSON.parse(localData) : []
  })

  useEffect(() => {
    localStorage.setItem('videos', JSON.stringify(videos))
  }, [videos] )
  
  return(
    <AppContext.Provider value={{videos, dispatch}}>
      { props.children }
    </AppContext.Provider>
  )
}

export default AppContextProvider