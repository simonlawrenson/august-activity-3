import React from "react"
import PageContainer from "./PageContainer"
import Header from "./Header"
import Footer from "./Footer"

const Layout = ({ children }) => {
  return (
    <div className="wrapper">
      <Header />
      <PageContainer>
        { children }
      </PageContainer>
      <Footer />
    </div>
  )
}


export default Layout