import React, { Component } from "react"
import { Link } from "react-router-dom";
import { AppContext } from '../contexts/AppContext'
import VideoDetail from './Player/VideoDetail'
import VideoList from './Player/VideoList'

class LovedVideosContainer extends Component {
  static contextType = AppContext
  
  state = {
    videos : this.context.videos,
    selectedVideo: null
  }
  
  handleVideoSelect = (video) => {
    this.setState({
      selectedVideo : video
    })
  }
  
  render() {
    return(
      <div className="container--favourites">
        <VideoDetail video={this.state.selectedVideo}/>
        { !this.state.videos.length >= 1 &&
          <div class="grid__container">
            <div className="grid__item">
              <p>You have not saved any videos yet. Start <Link to="/search" className="navigation__link">searching</Link> now.</p>
            </div>
          </div>
        }
        <VideoList handleVideoSelect={this.handleVideoSelect} videos={this.state.videos} isLocal={true}/>
      </div>
    )
  }
}

export default LovedVideosContainer