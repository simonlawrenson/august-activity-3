import React, {Component} from 'react';
import Layout from "./components/Layout"
import Index from "./pages/Index"
import Search from "./pages/Search"
import LovedVideos from "./pages/LovedVideos"
import AppContextProvider from "./contexts/AppContext"
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

class App extends Component {  
  render() {
    return (
      <Router>
        <AppContextProvider>
          <Layout>
            <Switch>
              <Route path="/" exact component={Index} />
              <Route path="/search" exact component={Search} />
              <Route path="/loved-videos" exact component={LovedVideos} />
            </Switch>
          </Layout>
        </AppContextProvider>
      </Router>
    )
  }
}

export default App;